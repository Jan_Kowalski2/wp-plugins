jQuery( function ( $ ) {
	'use strict';

	/**
	 * Update datetime picker element
	 * Used for static & dynamic added elements (when clone)
	 */
	function update() {
		var $this = $( this ),
			options = $this.data( 'options' ),
			$inline = $this.siblings( '.rwmb-datetime-inline' ),
			$timestamp = $this.siblings( '.rwmb-datetime-timestamp' ),
			current = $this.val(),
			$picker = $inline.length ? $inline : $this;

		$this.siblings( '.ui-datepicker-append' ).remove(); // Remove appended text
		if ( $timestamp.length ) {
			options.onClose = options.onSelect = function () {
				$timestamp.val( getTimestamp( $picker.datetimepicker( 'getDate' ) ) );
			};
		}

		if ( $inline.length ) {
			options.altField = '#' + $this.attr( 'id' );
			$this.on( 'keydown', _.debounce( function () {
				$picker
					.datepicker( 'setDate', $this.val() )
					.find( ".ui-datepicker-current-day" )
					.trigger( "click" );
			}, 600 ) );
			

			$inline
				.removeClass( 'hasDatepicker' )
				.empty()
				.prop( 'id', '' )
				.datetimepicker( options )
				.datetimepicker( 'setDate', current );
		}
		else {
			$this.removeClass( 'hasDatepicker' ).datetimepicker( options );
		}
	}

	/**
	 * Convert date to Unix timestamp in milliseconds
	 * @link http://stackoverflow.com/a/14006555/556258
	 * @param date
	 * @return number
	 */
	function getTimestamp( date ) {
		if ( date === null ) {
			return "";
		}
		var milliseconds = Date.UTC( date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds() );
		return Math.floor( milliseconds / 1000 );
	}

	// Set language if available
	$.timepicker.setDefaults( $.timepicker.regional[""] );
	if ( $.timepicker.regional.hasOwnProperty( RWMB_Datetime.locale ) ) {
		$.timepicker.setDefaults( $.timepicker.regional[RWMB_Datetime.locale] );
	}
	else if ( $.timepicker.regional.hasOwnProperty( RWMB_Datetime.localeShort ) ) {
		$.timepicker.setDefaults( $.timepicker.regional[RWMB_Datetime.localeShort] );
	}

	clearDatepicker();
	$( '.rwmb-datetime' ).each( update );
	$( document ).on( 'clone', '.rwmb-datetime', update );
	handleStartEndLessons();

	function clearDatepicker() {
		var old_fn = $.datepicker._updateDatepicker;
	 
		$.datepicker._updateDatepicker = function(inst) {
		   old_fn.call(this, inst);
	 
		   var buttonPane = $(this).datepicker("widget").find(".ui-datepicker-buttonpane");
	 
		   $("<button type='button' class='ui-datepicker-clean ui-state-default ui-priority-primary ui-corner-all'>Wyczyść</button>").appendTo(buttonPane).click(function(ev) {
			   $.datepicker._clearDate(inst.input);
			   // clear value here
		   }) ;
		}
	 }
	
	function handleStartEndLessons() {

		const submitButton = document.getElementById("publish");
		const form = document.getElementById("post");

		const startDescription = document.getElementById('_lp_time_limit_start-description');
		const endDescription = document.getElementById('_lp_time_limit_end-description');

		submitButton.addEventListener("click", function(e) {
			e.preventDefault();

			const specificStart = document.getElementById("_lp_time_limit_start");	
			const specificEnd = document.getElementById("_lp_time_limit_end");	
			const startMillis = Date.parse(specificStart.value);
			const endMillis = Date.parse(specificEnd.value);
			console.log(startMillis, endMillis, startMillis > endMillis)

			if (isNaN(startMillis) && endMillis) {
				clearErrorMessages(startDescription, endDescription);
				appendErrorMessage(startDescription, "Wprowadź prawidłową datę rozpoczęcia lekcji");
				return;
			}	

			if (isNaN(endMillis) && startMillis) {
				clearErrorMessages(startDescription, endDescription);
				appendErrorMessage(endDescription, "Wprowadź prawidłową datę zakończenia lekcji");
				return;
			}

			if (startMillis > endMillis) {
				clearErrorMessages(startDescription, endDescription);
				appendErrorMessage(endDescription, "Data rozpoczęcia nie może być późniejsza od daty zakończenia");
				return;
			}

			form.submit();
		});
	}

	function appendErrorMessage(element, message) {
		console.log(element, message);
		const content = document.createElement("span");
		content.textContent = message;
		element.parentNode.appendChild(content);
		content.classList.add("has-error");
	}
	
	function clearErrorMessages(startElem, endElem) {
		const errorContentStart = startElem.parentNode.querySelector("span")
		if (errorContentStart) {
			startElem.parentNode.removeChild(errorContentStart);
		}

		const errorContentEnd = endElem.parentNode.querySelector("span")
		if (errorContentEnd) {
			endElem.parentNode.removeChild(errorContentEnd);
		}
	}
} );
