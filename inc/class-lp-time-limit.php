<?php

/**
 * Class LP_Time_Limit
 */
class LP_Time_Limit {

	/**
	 * @var date
	 */
	public $time_limit_start = null;
	public $time_limit_end = null;

	/**
	 * LP_Time_Limit constructor.
	 *
	 */
	public function __construct( $time_limit_start, $time_limit_end ) {
		$this->time_limit_start = $time_limit_start;
		$this->time_limit_end = $time_limit_end;
	}

	public function __toString() {
		if ( is_date($this->time_limit_start) && is_date($this->time_limit_end) ) {
			return $this->time_limit_start . ' - ' . $this->time_limit_end;
		}
		return '';
	}

	public function get() {
		if ( strlen($this->time_limit_start) > 0 && strlen($this->time_limit_end) > 0 ) {
			return array($this->time_limit_start, $this->time_limit_end);
		}
		return null;
	}
}