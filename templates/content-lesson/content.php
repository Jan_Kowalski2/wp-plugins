<?php
/**
 * Template for displaying content of lesson.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-lesson/content.php.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.1
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
if ( ! isset( $item ) || ! isset( $content ) ) {
	return;
}

$start_date = DateTime::createFromFormat('U', strtotime($start));
$end_date = DateTime::createFromFormat('U', strtotime($start));
// $date_diff = date_diff($start_date, $end_date);

$is_current_time_after_start = strtotime($start) < strtotime('now');
$is_current_time_before_end = strtotime($end) > strtotime('now');

if (
	($is_current_time_after_start && !$is_current_time_before_end) ||
	(!$is_current_time_after_start && $is_current_time_before_end)
) {
	learn_press_get_template( 'content-lesson/no-content-available.php', array( 'lesson' => $item ) );

	return;
}

// lesson no content
if ( ! $content ) {
	learn_press_get_template( 'content-lesson/no-content.php', array( 'lesson' => $item ) );

	return;
}
?>

<div class="content-item-description lesson-description"><?php echo $content; ?></div>