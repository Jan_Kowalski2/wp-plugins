<?php
/**
 * Template for displaying info when lesson has time limits.
 *
 * @package  Learnpress/Templates
 * @version  3.0.1
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if ( is_null( $start ) || is_null($end) ) {
	return;
}
	
$start_date = DateTime::createFromFormat('U', strtotime($start));
$end_date = DateTime::createFromFormat('U', strtotime($start));
// $date_diff = date_diff($start_date, $end_date);

$is_current_time_after_start = strtotime($start) < strtotime('now');
$is_current_time_before_end = strtotime($end) > strtotime('now');

if ($is_current_time_after_start && $is_current_time_before_end) {
?>
<p class="time-limit-info"><?php echo "Lekcja trwa. Pozostań skupiony/a!" ?></p>

<?php
}
else if (!$is_current_time_after_start && $is_current_time_before_end) {
?>

<p class="time-limit-info"><?php echo "Lekcja jeszcze się nie rozpoczęła. Startujemy o: " . $start . "." ?></p>

<?php 
}
else if ($is_current_time_after_start && !$is_current_time_before_end) {
?>

<p class="time-limit-info"><?php echo "Lekcja jest zakończona." ?></p>
<?php } ?>